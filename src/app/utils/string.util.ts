import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StringUtil {
  toast_duration = 3000;

  cancel = 'Cancel';
  edit = 'Edit';
  delete = 'Delete';

  profileUpdateConfirmationTitle = 'Update Profile';
  profileUpdateConfirmationSubTitle = 'Are you sure want to save current change';

  can_change_later = 'You can change your election results before the election deadline is up';
  cannot_undo_message = 'This action cannot be undone';
  submit_data_message = 'Are you sure want to save this data ?';
  unsaved_data_message = 'Please save data first';
  unauthentication_error_message = 'Session expired, please login to continue';

  max_selected_item_message = 'You have reached the maximum selection limit';
  min_selected_item_message = 'You have not selected a single candidate';
  fix_selected_item_message = 'You still have selection';

  report_status_rejected = 'rejected';
  report_status_verified = 'verified';
  report_status_investigation_process = 'investigation_process';
  report_status_finished = 'finished';

  keydown_code_space = 32;
  keydown_code_up = 37;
  keydown_code_left = 38;
  keydown_code_down = 39;
  keydown_code_right = 40;

  failed_upload_title = 'Upload Gagal';
  failed_upload_message = 'Mohon ganti koneksi internet anda dengan yang lebih baik atau gunakan gambar berukuran kecil';

  event_vote_status_change = 'event_vote_status_change';

  event_on_search_votes = 'event_on_search_votes';
}
