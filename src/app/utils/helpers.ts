import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Environment } from './environment';
import { StringUtil } from './string.util';

@Injectable({ providedIn: 'root' })
export class Helpers {

  stringUtil: StringUtil = new StringUtil();

  constructor(
    private env: Environment,
  ) {
  }

  stringDateToNgbDate(stringDate: string): any {
    const splittedStringDate = stringDate.split('-');

    return {
      year: Number(splittedStringDate[0]),
      month: Number(splittedStringDate[1]),
      day: Number(splittedStringDate[2]),
    };
  }

  stringTimeToNgbTime(stringTime: string): any {
    const splittedStringTime = stringTime.split(':');

    return {
      hour: Number(splittedStringTime[0]),
      minute: Number(splittedStringTime[1]),
    };
  }

  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  getPage(totalPages: number, currentPage: number = 1) {
    // ensure current page isn't out of range
    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > totalPages) {
      currentPage = totalPages;
    }

    let startPage: number, endPage: number;

    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 0;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 0;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // console.log('start page = ' + Array.from(Array(endPage + 1 - startPage).keys()).map(
    //   i => startPage + i
    // ));

    return Array.from(Array(endPage - startPage).keys()).map(
      i => startPage + i
    );
  }

  months() {
    return [
      { id: 1, name: 'Januari', alias: 'Jan' },
      { id: 2, name: 'Februari', alias: 'Feb' },
      { id: 3, name: 'Maret', alias: 'Mar' },
      { id: 4, name: 'April', alias: 'Apr' },
      { id: 5, name: 'Mei', alias: 'Mei' },
      { id: 6, name: 'Juni', alias: 'Jun' },
      { id: 7, name: 'Juli', alias: 'Jul' },
      { id: 8, name: 'Agustus', alias: 'Agt' },
      { id: 9, name: 'September', alias: 'Sep' },
      { id: 10, name: 'Oktober', alias: 'Okt' },
      { id: 11, name: 'November', alias: 'Nov' },
      { id: 12, name: 'Desember', alias: 'Des' },
    ];
  }

  years() {
    return [
      { id: 2018 },
      { id: 2019 },
      { id: 2020 },
      { id: 2021 },
      { id: 2022 },
      { id: 2023 },
      { id: 2024 },
      { id: 2025 },
      { id: 2026 },
      { id: 2028 },
      { id: 2029 },
      { id: 2030 },
    ];
  }

  renaming(nameIT): any {
    const rename = [
      { name: 'material', aliases: 'Bahan' },
      { name: 'human', aliases: 'Orang' },
      { name: 'technique', aliases: 'Cara' },
      { name: 'tool', aliases: 'Alat' },
      { name: 'environment', aliases: 'Lingkungan' },
      { name: 'still_used', aliases: 'Tetap Dipakai' },
      { name: 'repair', aliases: 'Perbaiki' },
      { name: 'reducing_quality', aliases: 'Turunkan Mutu' },
      { name: 'disassemble', aliases: 'Bongkar & kerjakan ulang / buang' },
    ];
    const result = rename.find(x => x.name == nameIT).aliases;
    return result;
  }

  isAllowed(permissionName): boolean {
    // if (this.env.SECURITY_MODE === 'sso') {
    //     // const result = await this.ssoService.hasPermission(permissionName);
    //     // console.log('RESULT', result);
    //     return true;
    // } else {
    return JSON.parse(localStorage.getItem('userInfo')).permissions.find(obj => obj === permissionName);
    // }
  }

  rotateBase64Image90deg(base64Image, isClockwise) {
    // create an off-screen canvas
    // let loader = this.presentLoading();
    let offScreenCanvas = document.createElement('canvas');
    let offScreenCanvasCtx = offScreenCanvas.getContext('2d');

    // cteate Image
    var img = new Image();
    img.src = base64Image;

    // set its dimension to rotated size
    offScreenCanvas.height = img.width;
    offScreenCanvas.width = img.height;

    // rotate and draw source image into the off-screen canvas:
    if (isClockwise) {
      offScreenCanvasCtx.rotate(90 * Math.PI / 180);
      offScreenCanvasCtx.translate(0, -offScreenCanvas.width);
    } else {
      offScreenCanvasCtx.rotate(-90 * Math.PI / 180);
      offScreenCanvasCtx.translate(-offScreenCanvas.height, 0);
    }
    offScreenCanvasCtx.drawImage(img, 0, 0);

    let result = offScreenCanvas.toDataURL('image/jpeg', 80);

    // this.dismissLoading(loader);

    // encode image to data-uri with base64
    return result;
  }

  ngbDateToStringDate(ngbDateObject) {
    return `${ngbDateObject.year}-${ngbDateObject.month}-${ngbDateObject.day}`;
  }

  ngbTimeToStringTime(ngbTimeObject) {
    return `${ngbTimeObject.hour}:${ngbTimeObject.minute}`;
  }
}
