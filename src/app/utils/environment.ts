import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class Environment {

  mode = 'dev';

  APP_VERSION = '0.0.0-alpha';
  APP_NAME = 'Angular Template';

  SERVER_URL = '';
  CLIENT_ID = 2;
  CLIENT_SECRET = '';

  constructor() {
    switch (this.mode) {
      case 'dev':
        // this.SERVER_URL = 'http://192.168.100.52:8700/';
        this.SERVER_URL = 'http://localhost:8989/';
        this.CLIENT_ID = 2;
        this.CLIENT_SECRET = 'f1xMOFPfozjTl59VaTPMneZy5CseVZcBtptipWOb';
        break;
      case 'staging':
        this.SERVER_URL = '';
        this.CLIENT_ID = 2;
        this.CLIENT_SECRET = 'f1xMOFPfozjTl59VaTPMneZy5CseVZcBtptipWOb';
        break;
      case 'prod':
        this.SERVER_URL = '';
        this.CLIENT_ID = 2;
        this.CLIENT_SECRET = 'f1xMOFPfozjTl59VaTPMneZy5CseVZcBtptipWOb';
        break;
    }
  }
}
