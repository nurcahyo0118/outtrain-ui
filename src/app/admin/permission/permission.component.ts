import { PermissionService } from './../../services/permission.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'admin-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

  closeResult = '';

  permissions = [];

  isUpdateMode: boolean = false;

  constructor(
    private service: PermissionService,
  ) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.getAll().then(
      response => {

        const responseData = response.data;
        this.permissions = responseData.data;

      },
      error => {

      }
    );
  }

  onDeletePermission(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        this.service.delete(id).then(
          response => {
            const responseData = response.data;

            this.permissions.splice(this.permissions.findIndex(c => c.id == id), 1);

            Swal.fire(
              'Deleted!',
              'Your data has been deleted.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

}
