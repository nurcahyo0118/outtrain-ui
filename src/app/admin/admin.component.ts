import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'admin-root',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  title = 'social-job-vacancy';

  public isCollapsed = true;
  private lastPoppedUrl: string;
  private yScrollStack: number[] = [];

  ngOnInit() {
    $('.dashboard-responsive-nav-trigger').on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');

      var dashboardNavContainer = $('body').find(".dashboard-nav");

      if ($(this).hasClass('active')) {
        $(dashboardNavContainer).addClass('active');
      } else {
        $(dashboardNavContainer).removeClass('active');
      }

    });
  }
}
