import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'admin-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  showUserMenu: boolean = false;

  closeResult = '';

  users = [];

  isUpdateMode: boolean = false;

  constructor(
    private service: UserService,
  ) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.getAll().then(
      response => {

        const responseData = response.data;
        this.users = responseData.data;

      },
      error => {

      }
    );
  }

  onChangeUserStatus(user, status) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!'
    }).then((result) => {
      if (result.value) {

        this.service.updateStatus(user.id, status, user).then(
          response => {
            const responseData = response.data;

            this.getData();

            Swal.fire(
              'Status Changed!',
              'Your data has been changed.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

  onChangeTrainerCertifiedStatus(user, isCertified) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!'
    }).then((result) => {
      if (result.value) {

        this.service.updateTrainerStatus(user.id, isCertified, user).then(
          response => {
            const responseData = response.data;

            this.getData();

            Swal.fire(
              'Trainer Status Changed!',
              'Your data has been changed.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

  onDeleteUser(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        this.service.delete(id).then(
          response => {
            const responseData = response.data;

            this.users.splice(this.users.findIndex(c => c.id == id), 1);

            Swal.fire(
              'Deleted!',
              'Your data has been deleted.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

  onToggleUserMenu() {
    this.showUserMenu = !this.showUserMenu;
  }
}
