import { OutTrainService } from './../../services/out-train.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'admin-out-train',
  templateUrl: './out-train.component.html',
  styleUrls: ['./out-train.component.scss']
})
export class OutTrainComponent implements OnInit {

  closeResult = '';

  outTrains = [];

  isUpdateMode: boolean = false;

  constructor(
    private service: OutTrainService,
  ) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.getAll().then(
      response => {

        const responseData = response.data;
        this.outTrains = responseData.data;

      },
      error => {

      }
    );
  }

  onDeleteOutTrain(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        this.service.delete(id).then(
          response => {
            const responseData = response.data;

            this.outTrains.splice(this.outTrains.findIndex(c => c.id == id), 1);

            Swal.fire(
              'Deleted!',
              'Your data has been deleted.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

  onChangeStatus(item, status) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!'
    }).then((result) => {
      if (result.value) {

        this.service.updateStatus(item.id, status, item).then(
          response => {
            const responseData = response.data;

            this.getData();

            Swal.fire(
              'Status Changed!',
              'Your data has been changed.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

  onMarkAsFeatured(user, isFeatured) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, change it!'
    }).then((result) => {
      if (result.value) {

        this.service.updateFeaturedStatus(user.id, isFeatured, user).then(
          response => {
            const responseData = response.data;

            this.getData();

            Swal.fire(
              'Trainer Status Changed!',
              'Your data has been changed.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

}
