import { CategoryService } from './../../services/category.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'admin-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  showUserMenu: boolean = false;

  closeResult = '';

  categories = [];

  isUpdateMode: boolean = false;

  constructor(
    private service: CategoryService,
  ) {

  }

  ngOnInit() {
    // this.addCategories(10);
    this.getData();
  }

  getData() {
    this.service.getAll().then(
      response => {

        const responseData = response.data;
        this.categories = responseData.data;

      },
      error => {

      }
    );
  }

  addCategories(count) {

    for (let i = 0; i < count; i++) {
      this.categories.push({ name: `Category ${i + 1}`, id: i + 1 });
    }
  }

  onDeleteCategory(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        this.service.delete(id).then(
          response => {
            const responseData = response.data;

            this.categories.splice(this.categories.findIndex(c => c.id == id), 1);

            Swal.fire(
              'Deleted!',
              'Your data has been deleted.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

  onToggleUserMenu() {
    this.showUserMenu = !this.showUserMenu;
  }
}
