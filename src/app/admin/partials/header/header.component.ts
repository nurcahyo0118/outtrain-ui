import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'admin-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'social-job-vacancy';

  showUserMenu: boolean = false;
  userInfo: any;

  ngOnInit() {
    this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
    console.log('this.userInfo', this.userInfo);
  }

  onToggleUserMenu() {
    this.showUserMenu = !this.showUserMenu;
  }
}
