import { OutTrainFormComponent } from './out-train-form/out-train-form.component';
import { OutTrainComponent } from './out-train/out-train.component';
import { PermissionFormComponent } from './permission-form/permission-form.component';
import { PermissionComponent } from './permission/permission.component';
import { RoleFormComponent } from './role-form/role-form.component';
import { RoleComponent } from './role/role.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserComponent } from './user/user.component';
import { SharedModule } from './../shared/shared.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CategoryFormComponent } from './category-form/category-form.component';
import { CategoryComponent } from './category/category.component';
import { HeaderComponent } from './partials/header/header.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// Components
import { AdminComponent } from './admin.component';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';


const routes: Routes = [
    // { path: '', redirectTo: '/', pathMatch: 'full' },
    {
        path: '', component: AdminComponent, children: [
            { path: 'categories', component: CategoryComponent },
            { path: 'categories/create', component: CategoryFormComponent },
            { path: 'categories/:id/update', component: CategoryFormComponent },
            {
                path: 'user-management', children: [
                    { path: 'users', component: UserComponent },
                    { path: 'users/create', component: UserFormComponent },
                    { path: 'users/:id/update', component: UserFormComponent },
                    { path: 'roles', component: RoleComponent },
                    { path: 'roles/create', component: RoleFormComponent },
                    { path: 'roles/:id/update', component: RoleFormComponent },
                    { path: 'permissions', component: PermissionComponent },
                    { path: 'permissions/create', component: PermissionFormComponent },
                    { path: 'permissions/:id/update', component: PermissionFormComponent },
                ]
            },
            {
                path: 'out-train-management', children: [
                    { path: 'out-trains', component: OutTrainComponent },
                    { path: 'out-trains/create', component: OutTrainFormComponent },
                    { path: 'out-trains/:id/update', component: OutTrainFormComponent },
                ]
            }
        ]
    },
];

@NgModule({
    declarations: [
        AdminComponent,
        HeaderComponent,
        CategoryComponent,
        CategoryFormComponent,
        UserComponent,
        UserFormComponent,
        RoleComponent,
        RoleFormComponent,
        PermissionComponent,
        PermissionFormComponent,
        OutTrainComponent,
        OutTrainFormComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        NgbDropdownModule,
        NgbModalModule,
        NgxSpinnerModule,
        SharedModule,
    ],
    providers: [],
    bootstrap: [AdminComponent]
})
export class AdminModule { }
