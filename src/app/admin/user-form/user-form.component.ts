import { UserService } from './../../services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'admin-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  showUserMenu: boolean = false;

  closeResult = '';

  id: any;
  isUpdateMode = false;

  formGroup: FormGroup;
  apiValidationErrors;

  constructor(
    private service: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
  ) {
    // Get Params from route
    const routeParams = this.activeRoute.snapshot.params;
    this.id = routeParams.id;
  }

  ngOnInit() {
    this.initForm();
    this.getDataById(this.id);
  }

  initForm() {
    this.formGroup = this.formBuilder.group({
      id: ['', []],
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required]],
      address: ['', []],
      password: ['', []],
      photo: ['', []],
      email_verified_at: ['', []],
    });
  }

  getDataById(id) {
    if (id) {
      this.isUpdateMode = true;

      this.spinner.show();
      this.service.getOne(id).then(
        response => {
          this.spinner.hide();

          const responseData = response.data;
          console.log('responseData', responseData);

          this.formGroup.patchValue(responseData.data);

        },
        error => {
          this.spinner.hide();
        }
      );
    }
  }

  onToggleUserMenu() {
    this.showUserMenu = !this.showUserMenu;
  }

  onSubmit() {

    const requestData = JSON.parse(JSON.stringify(this.formGroup.value));

    Swal.fire({
      title: 'Save Data',
      // text: this.stringUtil.submit_data_message,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.value && this.isUpdateMode) {

        this.spinner.show();
        this.service.update(this.id, requestData).then(
          response => {
            this.spinner.hide();

            Swal.fire({
              title: 'Yay !',
              text: response.data.message,
              icon: 'success',
              confirmButtonText: 'Confirm'
            });

            this.router.navigate(['/admin/user-management/users']);
          },
          error => {
            this.spinner.hide();

            console.log(error);
            Swal.fire({
              title: 'Oops',
              text: error.error.message,
              icon: 'error',
              confirmButtonText: 'Confirm'
            });
          }
        );


      } else if (result.value) {


        this.spinner.show();
        this.service.store(requestData).then(
          response => {
            this.spinner.hide();

            Swal.fire({
              title: 'Yay !',
              text: response.data.message,
              icon: 'success',
              confirmButtonText: 'Confirm'
            });

            this.router.navigate(['/admin/user-management/users']);
          },
          error => {
            this.spinner.hide();

            console.log(error);
            Swal.fire({
              title: 'Oops',
              text: error.error.message,
              icon: 'error',
              confirmButtonText: 'Confirm'
            });
          }
        );


      }
    });
  }

}
