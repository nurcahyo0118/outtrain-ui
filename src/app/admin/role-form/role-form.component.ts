import { PermissionService } from './../../services/permission.service';
import { RoleService } from './../../services/role.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'admin-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.scss']
})
export class RoleFormComponent implements OnInit {

  closeResult = '';

  id: any;
  isUpdateMode = false;

  formGroup: FormGroup;
  apiValidationErrors;

  permissions = [];
  selectedPermissions = [];

  constructor(
    private service: RoleService,
    private permissionService: PermissionService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
  ) {
    // Get Params from route
    const routeParams = this.activeRoute.snapshot.params;
    this.id = routeParams.id;
  }

  ngOnInit() {
    this.initForm();
    this.getDataById(this.id);
  }

  initForm() {
    this.formGroup = this.formBuilder.group({
      id: ['', []],
      name: ['', [Validators.required]],
    });
  }

  getDataById(id) {

    this.permissionService.getAll().then(
      response => {
        const responseData = response.data;
        this.permissions = responseData.data;
      },
      error => {

      }
    )

    if (id) {
      this.isUpdateMode = true;

      this.spinner.show();
      this.service.getOne(id).then(
        response => {
          this.spinner.hide();

          const responseData = response.data;
          console.log('responseData', responseData);

          this.formGroup.patchValue(responseData.data);

          if(responseData.data.permissions) {
            this.selectedPermissions = responseData.data.permissions;
          } else {
            this.selectedPermissions = [];
          }

        },
        error => {
          this.spinner.hide();
        }
      );
    }
  }

  onSubmit() {

    const requestData = JSON.parse(JSON.stringify(this.formGroup.value));
    requestData.permissions = this.selectedPermissions;

    Swal.fire({
      title: 'Save Data',
      // text: this.stringUtil.submit_data_message,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.value && this.isUpdateMode) {

        this.spinner.show();
        this.service.update(this.id, requestData).then(
          response => {
            this.spinner.hide();

            Swal.fire({
              title: 'Yay !',
              text: response.data.message,
              icon: 'success',
              confirmButtonText: 'Confirm'
            });

            this.router.navigate(['/admin/user-management/roles']);
          },
          error => {
            this.spinner.hide();

            console.log(error);
            Swal.fire({
              title: 'Oops',
              text: error.error.message,
              icon: 'error',
              confirmButtonText: 'Confirm'
            });
          }
        );


      } else if (result.value) {


        this.spinner.show();
        this.service.store(requestData).then(
          response => {
            this.spinner.hide();

            Swal.fire({
              title: 'Yay !',
              text: response.data.message,
              icon: 'success',
              confirmButtonText: 'Confirm'
            });

            this.router.navigate(['/admin/user-management/roles']);
          },
          error => {
            this.spinner.hide();

            console.log(error);
            Swal.fire({
              title: 'Oops',
              text: error.error.message,
              icon: 'error',
              confirmButtonText: 'Confirm'
            });
          }
        );


      }
    });
  }

  onSelect(permission) {
    if (typeof this.selectedPermissions.find((childObject) => childObject.name == permission.name) === 'undefined') {
      this.selectedPermissions.push(permission);
    } else {
      this.selectedPermissions.forEach((childObject, childIndex) => {
        if (childObject.name == permission.name) {
          this.selectedPermissions.splice(childIndex, 1);
          return childObject;
        }
      });
    }
  }

  isSelected(permissionName): boolean {
    if (!this.selectedPermissions) {
      return false;
    }

    return this.selectedPermissions.findIndex(p => p.name == permissionName) > -1;
  }
}
