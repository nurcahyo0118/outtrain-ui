import { RoleService } from './../../services/role.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'admin-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  closeResult = '';

  roles = [];

  isUpdateMode: boolean = false;

  constructor(
    private service: RoleService,
  ) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.getAll().then(
      response => {

        const responseData = response.data;
        this.roles = responseData.data;

      },
      error => {

      }
    );
  }

  onDeleteRole(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        this.service.delete(id).then(
          response => {
            const responseData = response.data;

            this.roles.splice(this.roles.findIndex(c => c.id == id), 1);

            Swal.fire(
              'Deleted!',
              'Your data has been deleted.',
              'success'
            );

          },
          error => {

          }
        );

      }
    })
  }

}
