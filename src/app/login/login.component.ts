import { Router } from '@angular/router';
import { NativeAuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user = {
    username: '',
    password: ''
  }

  constructor(
    private spinner: NgxSpinnerService,
    private nativeAuthService: NativeAuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  onLogin() {

    if (this.user.username == null || this.user.password == null || this.user.username == '' || this.user.password == '') {

      Swal.fire({
        icon: 'warning',
        title: 'Oops',
        text: 'Username and Password must not be blank',
        heightAuto: false,
      });

    } else {
      this.spinner.show();
      this.nativeAuthService.login(this.user).then(
        response => {
          console.log('response.data', response.data);

          this.spinner.hide();

          const data = response.data;
          localStorage.setItem('credentials', JSON.stringify(data));

          this.router.navigate(['/admin']);
        },
        error => {
          console.log('error.response.data', error.response.data);

          this.spinner.hide();
          const data = error.response.data;

          Swal.fire(
            'Oops',
            data.message,
            'error'
          );
        }
      );
    }
  }

}
