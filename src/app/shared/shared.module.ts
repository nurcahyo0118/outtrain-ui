import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormErrorMessageComponent } from '../utils/form-error-message/form-error-message.component';

@NgModule({
    declarations: [
        FormErrorMessageComponent,
    ],
    imports: [
        CommonModule
    ],
    exports: [
        FormErrorMessageComponent,
    ],
})
export class SharedModule { }
