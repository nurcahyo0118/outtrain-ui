import { Component } from '@angular/core';

@Component({
  selector: 'landing-root',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent {
  title = 'social-job-vacancy';

  public isCollapsed = true;
  private lastPoppedUrl: string;
  private yScrollStack: number[] = [];
}
