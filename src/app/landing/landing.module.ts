import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// Components
import { LandingComponent } from './landing.component';

const routes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    {
        path: '', component: LandingComponent, children: [
        ]
    },
];

@NgModule({
    declarations: [
        LandingComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    ],
    providers: [],
    bootstrap: [LandingComponent]
})
export class LandingModule { }
