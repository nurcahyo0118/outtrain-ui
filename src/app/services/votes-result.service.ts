import { httpAuthClient } from './../network/http-client';
import { AxiosPromise } from 'axios';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class VotesResultService {

    constructor() {

    }

    getAll(voteId, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/monitor/votes/${voteId}/candidates`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllByKeyword(keyword: string, voteId, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${voteId}/results/search/${keyword}`, {
            params: {
                'page': page.toString()
            }
        });
    }
}
