import { httpAuthClient } from './../network/http-client';
import { Injectable } from '@angular/core';

import { AxiosPromise } from 'axios';

@Injectable({ providedIn: 'root' })
export class OutTrainService {

    constructor() {

    }

    store(item): AxiosPromise<any> {

        return httpAuthClient.post(`api/v1/out-trains/store`, item);
    }

    update(id, item): AxiosPromise<any> {

        return httpAuthClient.put(`api/v1/out-trains/${id}/update`, item);
    }

    updateStatus(id, status, item): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/out-trains/${id}/update-status/${status}`, item);
    }

    updateFeaturedStatus(id, status, item): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/out-trains/${id}/update-featured-status/${status}`, item);
    }

    getOne(id): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/out-trains/${id}`);
    }

    getAll(page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/out-trains`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllByKeyword(keyword: string, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/out-trains/search/${keyword}`, {
            params: {
                'page': page.toString()
            }
        });
    }

    delete(id): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/out-trains/${id}/delete`);
    }

    deleteMultiple(ids: number[]): Promise<any> {

        return httpAuthClient.delete(`api/v1/out-trains/delete/multiple`, {
            params: {
                'ids': JSON.stringify(ids)
            }
        }).then(
            response => {
                return response;
            },
            error => {
                return error;
            }
        );
    }

}
