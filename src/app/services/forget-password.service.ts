import { httpClient } from './../network/http-client';
import { Injectable } from '@angular/core';

import { AxiosPromise } from 'axios';

@Injectable({ providedIn: 'root' })
export class ForgetPasswordService {

    forgetPassword(requestBody): AxiosPromise<any> {
        return httpClient.post('api/v1/forget-password', requestBody);
    }

    resetPassword(requestBody): AxiosPromise<any> {
        return httpClient.post('api/v1/reset-password', requestBody);
    }

}