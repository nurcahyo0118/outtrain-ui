import { httpAuthClient } from './../network/http-client';
import { Injectable } from '@angular/core';

import { AxiosPromise } from 'axios';

@Injectable({providedIn: 'root'})
export class MeService {

  constructor() {
  }

  getCurrentUser(): AxiosPromise<any> {
    return httpAuthClient.get('api/v1/me');
  }

  getCurrentUserUnits(): AxiosPromise<any> {
    return httpAuthClient.get('api/v1/me/units');
  }

  updateCurrentUser(user): AxiosPromise<any> {
    return httpAuthClient.put('api/v1/me/update', user);
  }

  updateCurrentUserPassword(item: any): AxiosPromise<any> {
    return httpAuthClient.put('api/v1/me/update/password', {
      current_password: item.currentPassword,
      new_password: item.newPassword
    });
  }

  updateCurrentUserPhoto(photo: File, config: any): AxiosPromise<any> {
    const formData = new FormData();
    formData.append('photo', photo);

    return httpAuthClient.post('api/v1/me/update/photo', formData, config);
  }

  getAllCurrentUserBanners(orderBy = 'created_at', orderType = 'desc', filters = {}): AxiosPromise<any> {
    return httpAuthClient.get(`api/v1/me/banners`, {
      params: {
        filters,
        order_by: orderBy,
        order_type: orderType,
        with: []
      }
    });
  }

  getAllCurrentUserMessages(orderBy = 'created_at', orderType = 'desc', filters = {}): AxiosPromise<any> {
    return httpAuthClient.get(`api/v1/me/messages`, {
      params: {
        filters,
        order_by: orderBy,
        order_type: orderType,
        with: []
      }
    });
  }

  getAllNotificationsGroupByProjects(orderBy = 'created_at', orderType = 'desc', filters = {}): AxiosPromise<any> {
    return httpAuthClient.get(`api/v1/me/notifications/group-by-projects`, {
      params: {
        filters,
        order_by: orderBy,
        order_type: orderType,
        with: []
      }
    });
  }

  getAllNotifications(limit = 0, orderBy = 'created_at', orderType = 'desc', filters = {}): AxiosPromise<any> {
    return httpAuthClient.get(`api/v1/me/notifications`, {
      params: {
        filters,
        order_by: orderBy,
        order_type: orderType,
        with: [],
        limit
      }
    });
  }

  getNotifications(page = 1, filterValue = 'all', orderBy = 'created_at', orderType = 'desc', filters = {}): AxiosPromise<any> {
    return httpAuthClient.get(`api/v1/me/notifications/pages`, {
      params: {
        filters,
        order_by: orderBy,
        order_type: orderType,
        with: [],
        page,
        filter_value: filterValue,
      }
    });
  }

  getMessages(page = 1, orderBy = 'created_at', orderType = 'desc', filters = {}): AxiosPromise<any> {
    return httpAuthClient.get(`api/v1/me/messages/pages`, {
      params: {
        filters,
        order_by: orderBy,
        order_type: orderType,
        with: [],
        page,
      }
    });
  }

  readNotification(notificationId): AxiosPromise<any> {
    return httpAuthClient.post(`api/v1/me/notifications/${notificationId}/read`, null);
  }
}
