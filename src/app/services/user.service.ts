import { httpAuthClient } from './../network/http-client';
import { AxiosPromise } from 'axios';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UserService {

    constructor() {

    }

    store(item): AxiosPromise<any> {

        return httpAuthClient.post(`api/v1/users/store`, item);
    }

    update(id, item): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/users/${id}/update`, item);
    }

    updateStatus(id, status, item): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/users/${id}/update-status/${status}`, item);
    }

    updateTrainerStatus(id, status, item): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/users/${id}/update-trainer-status/${status}`, item);
    }

    getOne(id): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/users/${id}`);
    }

    getAll(page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/users`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllByKeyword(keyword: string, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/users/search/${keyword}`, {
            params: {
                'page': page.toString()
            }
        });
    }

    delete(id): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/users/${id}/delete`);
    }

    deleteMultiple(ids: number[]): Promise<any> {

        return httpAuthClient.delete(`api/v1/users/delete/multiple`, {
            params: {
                'ids': JSON.stringify(ids)
            }
        }).then(
            response => {
                return response;
            },
            error => {
                return error;
            }
        );
    }
}
