import { AxiosPromise } from 'axios';
import { httpAuthClient } from './../network/http-client';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class VotesMonitoringService {
    constructor() {
    }

    getAllCandidates(voteId, page = 1): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/monitor/votes/${ voteId }/candidates`, {
            params: {
                page: page.toString()
            }
        });
    }
}
