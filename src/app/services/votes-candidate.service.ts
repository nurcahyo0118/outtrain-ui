import { httpAuthClient } from './../network/http-client';
import { Injectable } from '@angular/core';
import { AxiosPromise } from 'axios';


@Injectable({ providedIn: 'root' })
export class VotesCandidateService {

    constructor() {

    }

    store(item, voteId): AxiosPromise<any> {

        return httpAuthClient.post(`api/v1/votes/${voteId}/candidates/store/pair`, item);
    }

    update(item, voteId, candidateId): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/votes/${voteId}/candidates/${candidateId}/update/pair`, item);
    }

    storeMember(item, voteId, candidateId): AxiosPromise<any> {

        return httpAuthClient.post(`api/v1/votes/${voteId}/candidates/${candidateId}/members/store`, item);
    }

    updateMember(item, memberId): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/members/${memberId}/update`, item);
    }

    deleteMember(memberId): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/candidate-members/${memberId}/delete`);
    }

    getOne(candidateId, voteId): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${voteId}/candidates/${candidateId}`);
    }

    getAllWithoutPagination(voteId): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${voteId}/candidates/no-paginaton`);
    }

    getAll(voteId, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${voteId}/candidates`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllMembersByCandidateId(candidateId): AxiosPromise<any> {
        return httpAuthClient.get(
            `api/v1/candidates/${candidateId}/members`,
            {
                params: {
                    all: true
                }
            }
        );
    }

    getAllByKeyword(keyword: string, voteId, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${voteId}/candidates/search/${keyword}`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllByKeywordWithoutPagination(keyword: string, voteId): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${voteId}/candidates/search/${keyword}/no-paginaton`);
    }

    delete(candidateId: number, voteId: number): AxiosPromise<any> {

        return httpAuthClient.delete(`api/v1/votes/${voteId}/candidates/${candidateId}/delete`);
    }

    deleteMultiple(voteId: number, ids: number[]): AxiosPromise<any> {

        return httpAuthClient.delete(`api/v1/votes/${voteId}/candidates/delete/multiple`, {
            params: {
                'ids': JSON.stringify(ids)
            }
        });
    }

    // Tokens
    getAllWithoutPaginationByToken(voteId): AxiosPromise<any> {

        const credentials = JSON.parse(localStorage.getItem('credentials'));
        return httpAuthClient.get(`api/v1/tokens/votes/${voteId}/candidates/no-paginaton`, {
            headers: {
                'vote_token': credentials.token
            }
        });
    }

    // uploadTemp(photo: File, config: any): AxiosPromise<any> {
    //     const formData = new FormData();
    //     formData.append('photo', photo);
    //     return httpAuthClient.post('api/v1/file-uploads/temp', formData, config);
    // }

}
