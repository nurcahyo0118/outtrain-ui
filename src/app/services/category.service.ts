import { httpAuthClient } from './../network/http-client';
import { AxiosPromise } from 'axios';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CategoryService {

    constructor() {

    }

    store(item): AxiosPromise<any> {

        return httpAuthClient.post(`api/v1/categories/store`, item);
    }

    update(id, item): AxiosPromise<any> {

        return httpAuthClient.put(`api/v1/categories/${id}/update`, item);
    }

    getOne(id): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/categories/${id}`);
    }

    getAll(page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/categories`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllByKeyword(keyword: string, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/categories/search/${keyword}`, {
            params: {
                'page': page.toString()
            }
        });
    }

    delete(id): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/categories/${id}/delete`);
    }

    deleteMultiple(ids: number[]): Promise<any> {

        return httpAuthClient.delete(`api/v1/categories/delete/multiple`, {
            params: {
                'ids': JSON.stringify(ids)
            }
        }).then(
            response => {
                return response;
            },
            error => {
                return error;
            }
        );
    }
}
