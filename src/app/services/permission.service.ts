import { httpAuthClient } from './../network/http-client';
import { Injectable } from '@angular/core';

import { AxiosPromise } from 'axios';

@Injectable({ providedIn: 'root' })
export class PermissionService {

    constructor() {

    }

    store(item): AxiosPromise<any> {

        return httpAuthClient.post(`api/v1/permissions/store`, item);
    }

    update(id, item): AxiosPromise<any> {

        return httpAuthClient.put(`api/v1/permissions/${id}/update`, item);
    }

    getOne(id): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/permissions/${id}`);
    }

    getAll(page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/permissions`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllByKeyword(keyword: string, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/permissions/search/${keyword}`, {
            params: {
                'page': page.toString()
            }
        });
    }

    delete(id): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/permissions/${id}/delete`);
    }

    deleteMultiple(ids: number[]): Promise<any> {

        return httpAuthClient.delete(`api/v1/permissions/delete/multiple`, {
            params: {
                'ids': JSON.stringify(ids)
            }
        }).then(
            response => {
                return response;
            },
            error => {
                return error;
            }
        );
    }

}
