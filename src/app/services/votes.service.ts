import { AxiosPromise } from 'axios';
import { httpAuthClient } from './../network/http-client';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class VotesService {
    constructor() {
    }

    store(item): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/store`, item);
    }

    update(id, item): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/votes/${id}/update`, item);
    }

    getOne(id): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${id}`);
    }

    getOneBySlug(slug): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/by-slug/${slug}`);
    }

    getOneWithMyResult(id): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${id}/my-result`);
    }

    getTokenCountAll(id): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${id}/tokens/counts`);
    }

    getTokenActiveCountAll(id): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${id}/tokens/active/counts`);
    }

    getTokenUsedCountAll(id): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${id}/tokens/used/counts`);
    }

    getCandidatesCountAll(id): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${id}/candidates/counts`);
    }

    getCandidateSelectedCountAll(id, candidateId): AxiosPromise<any> {
        return httpAuthClient.get(
            `api/v1/votes/${id}/candidates/${candidateId}/selected/counts`
        );
    }

    getAll(page = 1): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes`, {
            params: {
                page: page.toString()
            }
        });
    }

    getAllByMe(page = 1): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/me/votes`, {
            params: {
                page: page.toString()
            }
        });
    }

    getAllByUserId(userId, page = 1): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/users/${userId}/votes`, {
            params: {
                page: page.toString()
            }
        });
    }

    getAllPublished(page = 1, filters = null): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/published`, {
            params: {
                page: page.toString(),
                filters
            }
        });
    }

    getAllByKeyword(keyword: string, page = 1): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/search/${keyword}`, {
            params: {
                page: page.toString()
            }
        });
    }

    getAllPublishedByKeyword(keyword: string, page = 1, filters = null): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/search/published/${keyword}`, {
            params: {
                page: page.toString(),
                filters
            }
        });
    }

    delete(id: number): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/votes/${id}/delete`);
    }

    deleteMultiple(ids: number[]): Promise<any> {
        return httpAuthClient
            .delete(`api/v1/votes/delete/multiple`, {
                params: {
                    ids: JSON.stringify(ids)
                }
            })
            .then(
                response => {
                    return response;
                },
                error => {
                    return error;
                }
            );
    }

    changeStatus(id, status): AxiosPromise<any> {
        return httpAuthClient.put(`api/v1/votes/${id}/change-status/${status}`, null);
    }

    publish(id): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/${id}/publish`, null);
    }

    revert(id): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/${id}/revert`, null);
    }

    postElectionResult(id, data): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/${id}/election/result`, data);
    }

    postElectionResultByToken(id, data): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/tokens/votes/${id}/election/result`, {
            candidates: data
        });
    }

    // updateCurrentVoteImage(id, image: File): AxiosPromise<any> {
    //     const formData = new FormData();
    //     formData.append('image', image);
    //     return httpAuthClient.post(`api/v1/votes/${id}/update/image`, formData, {
    //         reportProgress: true,
    //         observe: 'events'
    //     });
    // }

    uploadTemp(photo: File, config: any): AxiosPromise<any> {
        const formData = new FormData();
        formData.append('photo', photo);
        return httpAuthClient.post('api/v1/file-uploads/temp', formData, config);
    }

}
