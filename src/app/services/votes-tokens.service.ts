import { httpAuthClient } from './../network/http-client';
import { AxiosPromise } from 'axios';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class VotesTokensService {

    constructor() {

    }

    store(item, voteId): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/${ voteId }/tokens/store`, item);
    }

    getOne(voteId): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/tokens/votes/${ voteId }`);
    }

    getOneBySlug(slug): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/tokens/votes/by-slug/${ slug }`);
    }

    getAll(voteId, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${ voteId }/tokens`, {
            params: {
                'page': page.toString()
            }
        });
    }

    getAllByKeyword(keyword: string, voteId, page = 1): AxiosPromise<any> {

        return httpAuthClient.get(`api/v1/votes/${ voteId }/tokens/search/${ keyword }`, {
            params: {
                'page': page.toString()
            }
        });
    }

    delete(tokenId: number, voteId: number): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/votes/${ voteId }/tokens/${ tokenId }/delete`);
    }

    deleteAll(voteId: number): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/votes/${ voteId }/tokens/delete`);
    }

    // Vote by token
    postElectionResultByToken(id, data): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/tokens/votes/${ id }/election/result`, data);
    }

    getVoteByToken(voteId): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/tokens/votes/${ voteId }`);
    }

    getAllCandidatesByToken(voteId): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/tokens/votes/${ voteId }/candidates/no-paginaton`);
    }

    activateToken(tokenId: number, voteId: number) {
        return httpAuthClient.post(`api/v1/votes/${ voteId }/tokens/${ tokenId }/activate`, null);
    }

    deactivateToken(tokenId: number, voteId: number) {
        return httpAuthClient.post(`api/v1/votes/${ voteId }/tokens/${ tokenId }/deactivate`, null);
    }
}
