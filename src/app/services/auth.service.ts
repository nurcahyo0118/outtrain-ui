import { httpClient, httpAuthClient } from './../network/http-client';
import { Injectable } from '@angular/core';

import { Environment } from '../utils/environment';
import { AxiosPromise } from 'axios';

@Injectable({ providedIn: 'root' })
export class NativeAuthService {
  constructor(
    private env: Environment
  ) {
  }

  loginWithSocialCredentials(data): AxiosPromise<any> {
    return httpClient.post(
      'api/v1/login/social/secret',
      data
    );
  }

  loginSocial(data): AxiosPromise<any> {
    return httpClient.post(
      'api/v1/login/social',
      data
    );
  }

  checkAuth(): AxiosPromise<any> {
    return httpAuthClient.get('api/v1/me/auth');
  }

  login(user): AxiosPromise<any> {
    return httpClient.post('oauth/token', {
      grant_type: 'password',
      client_id: this.env.CLIENT_ID,
      client_secret: this.env.CLIENT_SECRET,
      username: user.username,
      password: user.password,
      scope: ''
    });
  }

  refreshToken(): AxiosPromise<any> {
    return httpAuthClient.post('oauth/token', {
      grant_type: 'refresh_token',
      refresh_token: `${JSON.parse(localStorage.getItem('credentials')).refresh_token}`,
      client_id: this.env.CLIENT_ID,
      client_secret: this.env.CLIENT_SECRET,
      scope: ''
    });
  }

  logout(): AxiosPromise<any> {
    return httpAuthClient.post(`api/v1/me/logout`, {});
  }

  register(user): AxiosPromise<any> {
    return httpClient.post(`api/v1/register`, user);
  }
}
