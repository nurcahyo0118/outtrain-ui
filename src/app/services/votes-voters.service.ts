import { httpAuthClient } from './../network/http-client';
import { AxiosPromise } from 'axios';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class VotesVotersService {
    constructor() {
    }

    isJoin(voteId, userId): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${voteId}/voters/isjoin/${userId}`);
    }

    join(voteId, userId): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/${voteId}/voters/join/${userId}`, null);
    }

    quit(voteId, userId): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/${voteId}/voters/quit/${userId}`, null);
    }

    store(item, voteId): AxiosPromise<any> {
        return httpAuthClient.post(`api/v1/votes/${voteId}/voters/store`, item);
    }

    update(voterId, item, voteId): AxiosPromise<any> {
        return httpAuthClient.put(
            `api/v1/votes/${voteId}/voters/${voterId}/update`,
            item
        );
    }

    getOne(voterId, voteId): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${voteId}/voters/${voterId}`);
    }

    getAll(voteId, page = 1): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${voteId}/voters`, {
            params: {
                page: page.toString()
            }
        });
    }

    countAll(voteId): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${voteId}/voters/count`);
    }

    getAllByKeyword(keyword: string, voteId, page = 1): AxiosPromise<any> {
        return httpAuthClient.get(`api/v1/votes/${voteId}/voters/search/${keyword}`, {
            params: {
                page: page.toString()
            }
        });
    }

    delete(voterId: number, voteId: number): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/votes/${voteId}/voters/${voterId}/delete`);
    }

    deleteMultiple(voteId: number, ids: number[]): AxiosPromise<any> {
        return httpAuthClient.delete(`api/v1/votes/${voteId}/voters/delete/multiple`, {
            params: {
                ids: JSON.stringify(ids)
            }
        });
    }
}
